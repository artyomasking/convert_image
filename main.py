# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
from PIL import Image
import glob


def revert():
    image_open = glob.glob('_good/*.jpg')
    for i in image_open:
        print(i)
        link = i
        link_good = f'image/{link}'
        image = Image.open(link)  # Открываем изображение
        if image.mode != 'RGB':
            image = image.convert('RGB')
        size_im = (round(image.size[0]/2), round(image.size[1]/2))
        im_rotate = image.rotate(30, expand=True, fillcolor=(255, 255, 255))
        im_rotate.save(link_good, quality=95)
        image.close()

        im1 = Image.open(link_good)
        im2 = Image.open('watermark/watermark.jpg')
        siz2_wat = (size_im[0], round(im2.size[1] / (im2.size[0] / size_im[0])))

        im1.paste(im2.resize(siz2_wat))
        im1.save(link_good, quality=95)

        im1.close()
        im2.close()



# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    revert()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
